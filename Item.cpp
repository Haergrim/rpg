#include "Item.h"

/// \fn Item()
/// \brief Empty constructor
Item::Item()
{
    //ctor
}

Item::~Item()
{
    //dtor
}

/// \fn void addToInv()
/// \brief Function used to add items to a character's inventory (not implemented yet)
void Item::addToInv()
{
    //A coder
}

/// \fn void sell()
/// \brief Function used to sell an item from a character's inventory (not implemented yet)
void Item::sell()
{
    //A coder
}

/// \fn sf::String getName()
/// \brief Returns an item name
sf::String Item::getName()
{
    return name;
}

/// \fn sf::String getDescription()
/// \brief Returns an item description
sf::String Item::getDescription()
{
    return description;
}

/// \fn int getValue()
/// \brief Returns an item value
int Item::getValue()
{
    return value;
}
