#ifndef PC_H
#define PC_H

#include <vector>

#include "Character.h"
#include "Weapon.h"
#include "Armor.h"
#include "Miscellanous.h"

/// \class PC PC.h
/// \brief Class used to describe a playable character
class PC : public Character
{
    public:
        PC();
        virtual ~PC();

        void equipWeap(Weapon *weapToEquip);
        void equipArmor(Armor *armorToEquip);
        Weapon getEquipWeapon();
        Armor getEquipArmor();
        void addXP(int xp);

        std::vector<Weapon*> inventoryWeap;
        std::vector<Armor*> inventoryArmor;
        std::vector<Miscellanous*> inventoryMisc;

    protected:

    private:
        Weapon *equippedWeap;
        Armor *equippedArmor;
};

#endif // PC_H
