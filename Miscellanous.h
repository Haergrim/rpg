#ifndef MISCELLANOUS_H
#define MISCELLANOUS_H

#include <SFML/Graphics.hpp>
#include "Item.h"

/// \class Miscellanous Miscellanous.h
/// \brief Class used to describe various objects which aren't a weapon or an armor
class Miscellanous : public Item
{
    public:
        Miscellanous();
        virtual ~Miscellanous();

        void heal();
        void buff();

    protected:
        int healingValue;

    private:

};

#endif // MISCELLANOUS_H
