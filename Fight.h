#ifndef FIGHT_H
#define FIGHT_H

#include "PC.h"
#include "Monster.h"
#include <iostream>
#include "cScreen.h"
#include "stdlib.h"

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

class MainGame;

/// \class Fight Fight.h
/// \brief Class used to generate the fighting screen
class Fight : public cScreen
{
    public:
        Fight(MainGame *RPG);
        virtual ~Fight();
        virtual int run(sf::RenderWindow &window);

        void attack();
        void defend();
        void use();
        void winXP();
        sf::String iToStr(int i);
        void monsterDead();
        void playerDead();
        void attackMonster();

    protected:

    private:
        Monster *monsterInFight;
        float posx;
        float posy;
        float posx2;
        float posy2;
        float movement_step;
        sf::String infos;
        MainGame *RPG;
        int subMenu;
};

#endif // FIGHT_H
