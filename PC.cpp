#include "PC.h"

/// \fn PC()
/// \brief Constructor initializing the character's basic stats
PC::PC()
{
    //ctor
    name = "Haergrim";
    level = 1;
    maxHP = 50;
    currentHP = maxHP;
    maxMP = 0;
    currentMP = maxMP;
    strength = 8;
    constitution = 5;
    dexterity = 8;
    intelligence = 5;
    experience = 0;
    equippedWeap = new Weapon();
    equippedArmor = new Armor();
    Miscellanous *potion = new Miscellanous();
    inventoryWeap.push_back(equippedWeap);
    inventoryArmor.push_back(equippedArmor);
    inventoryMisc.push_back(potion);
    inventoryMisc.push_back(potion);
}

PC::~PC()
{
    //dtor
}

/// \fn void equipWeap(Weapon *weapToEquip)
/// \brief Change the actually equipped weapon to a new one
void PC::equipWeap(Weapon *weapToEquip)
{
    this->equippedWeap = weapToEquip;
}

/// \fn void equipArmor(Armor *armorToEquip)
/// \brief Change the actually equipped armor to a new one
void PC::equipArmor(Armor *armorToEquip)
{
    this->equippedArmor = armorToEquip;
}

/// \fn Weapon getEquipWeapon()
/// \brief Returns the currently equipped weapon (allow access to all weapon information)
Weapon PC::getEquipWeapon()
{
    return *equippedWeap;
}

/// \fn Weapon getEquipWeapon()
/// \brief Returns the currently equipped weapon (allow access to all weapon information)
Armor PC::getEquipArmor()
{
    return *equippedArmor;
}

/// \fn void addXP(int xp)
/// \brief Add a fixed amount of experience to the playable character's actual total
void PC::addXP(int xp)
{
    experience+= xp;
}

