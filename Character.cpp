#include "Character.h"

/// \fn Character()
/// \brief Empty constructor
Character::Character()
{
    //ctor
}

Character::~Character()
{
    //dtor
}

/// \fn sf::String getName()
/// \brief Returns a character's name
sf::String Character::getName()
{
    return name;
}

/// \fn void setName()
/// \brief Sets a character's name
void Character::setName(sf::String name)
{
    this->name = name;
}

/// \fn int getLvl()
/// \brief Returns a character's level
int Character::getLvl()
{
    return level;
}

/// \fn void lvlUp()
/// \brief Level Up a character (not implemented yet)
void Character::lvlUp()
{
    //A coder
}

/// \fn int getMaxHP()
/// \brief Returns a character's maxHP
int Character::getMaxHP()
{
    return maxHP;
}

/// \fn int getMaxMP()
/// \brief Returns a character's maxMP
int Character::getMaxMP()
{
    return maxMP;
}

/// \fn int getCurrentHP()
/// \brief Returns a character's current HP
int Character::getCurrentHP()
{
    return currentHP;
}

/// \fn int getCurrentMP()
/// \brief Returns a character's current MP
int Character::getCurrentMP()
{
    return currentMP;
}

/// \fn int getStr()
/// \brief Returns a character's strength
int Character::getStr()
{
    return strength;
}

/// \fn int getCons()
/// \brief Returns a character's constitution
int Character::getCons()
{
    return constitution;
}

/// \fn int getDex()
/// \brief Return a character's dexterity
int Character::getDex()
{
    return dexterity;
}

/// \fn int getInt()
/// \brief Return a character's intelligence
int Character::getInt()
{
    return intelligence;
}

/// \fn int getExp()
/// \brief Return a character's experience
int Character::getExp()
{
    return experience;
}

/// \fn int getPosX()
/// \brief Return a character current position on the X axis
int Character::getPosX()
{
    return posX;
}

/// \fn int getPosY()
/// \brief Return a character current position on the Y axis
int Character::getPosY()
{
    return posY;
}

/// \fn void setPos(int X, int Y)
/// \brief Sets a character current position (not in use)
void Character::setPos(int X, int Y)
{
    //a coder
}

/// \fn void looseHP(int dmg)
/// \brief Updates a character currentHP based on received damages
void Character::looseHP(int dmg)
{
    currentHP -= dmg;
}
