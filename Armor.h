#ifndef ARMOR_H
#define ARMOR_H

#include "Item.h"

/// \class Armor Armor.h
/// \brief Class used for chara's armor
class Armor : public Item
{
    public:
        Armor();
        virtual ~Armor();

        int getDef();

    protected:

    private:
        int defense;
};

#endif // ARMOR_H
