#ifndef MAINGAME_H
#define MAINGAME_H

#include <SFML/Graphics.hpp>
#include <iostream>

#include "PC.h"
#include "Monster.h"
#include "Weapon.h"
#include "Armor.h"
#include "Miscellanous.h"
#include "Loot.h"

#include "screens.h"

/// \class MainGame MainGame.h
/// \brief Class regrouping all game components
class MainGame
{
    public:
        MainGame();
        virtual ~MainGame();

        int launchGame();

        PC *player;
        sf::Clock clock;

    protected:

    private:



};

#endif // MAINGAME_H
