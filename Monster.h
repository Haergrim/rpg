#ifndef MONSTER_H
#define MONSTER_H

#include "Loot.h"
#include "Character.h"
#include <iostream>
#include "sqlite3.h"
#include <stdlib.h>

/// \class Monster Monster.h
/// \brief Class used to describe a generic monster
class Monster : public Character
{
    public:
        Monster();
        virtual ~Monster();

        int getGivenXp();
        sf::String getType();
        Loot getLoot();

    protected:

    private:
        Loot loot[2];
        int givenExp;
        sf::String type;
        int random;
};

#endif // MONSTER_H
