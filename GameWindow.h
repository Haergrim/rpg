#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <iostream>
#include <fstream>
#include "cScreen.h"

#include <SFML/Graphics.hpp>

/// \class GameWindow GameWindow.h
/// \brief Class used to generate the main game screen
class GameWindow : public cScreen
{
    public:
        GameWindow();
        virtual ~GameWindow();
        virtual int run(sf::RenderWindow &window);

    protected:

    private:
        float movement_step;
        float posx;
        float posy;
        float nextPosx;
        float nextPosy;
        sf::Sprite chara;
        sf::Sprite background;
        //Tiles size 25x25, our 800x600 window is then represented through 32x24 tiles
        int tiles[32][24];
        int subMenu;
};


#endif // GAMEWINDOW_H
