#include "GameWindow.h"
#include <string>

/// \fn GameWindow()
/// \brief Constructor charging the map informations from a .txt file
GameWindow::GameWindow()
{
	movement_step = 5;

	//Initialize tiles to 0
	for (int i = 0 ; i < 24 ; i++)
    {
        for (int j = 0 ; j < 32 ; j++)
        {
            tiles[i][j] = 0;
        }
    }

    //Parse .txt file to charge tiles infos
    std::string tmp;
    std::ifstream ifs;
    std::string tmp2;

    ifs.open("rsc/Maps/Map01.txt", std::ifstream::in);

    int x = 0;

    while(!ifs.eof())
    {
        std::getline(ifs, tmp);

        for (unsigned int y = 0 ; y < tmp.length() ; y++)
        {
            tmp2 = tmp[y];
            tiles[y][x] = atoi(tmp2.c_str());
        }

        x++;
    }

    ifs.close();

    //Chara's starting point
    for (int i = 0 ; i < 24 ; i++)
    {
        for (int j = 0 ; j < 32 ; j++)
        {
            if (tiles[j][i] == 2)
            {
                posx = j*25;
                posy = i*25;
            }
        }
    }

    //Init next step
    nextPosx = posx;
    nextPosy = posy;

    subMenu = 1;
}

GameWindow::~GameWindow()
{
    //dtor
}

/// \fn int run(sf::RenderWindow &window)
/// \brief Show screen and manages it's internal logic
int GameWindow::run(sf::RenderWindow &window)
{
    //Used variables
	sf::Event event;
	sf::Text info;
	sf::Font fontMenu;
	bool running = true;

    //Setting sprites (map + chara)
	sf::Texture texture;
	if (!texture.loadFromFile("rsc/CharaDown.png"))
	{
		std::cout << "Error loading chara" << std::endl;
		return (-1);
	}
	chara.setTexture(texture);

	sf::Texture texture2;
	if (!texture2.loadFromFile("rsc/Map_15.png"))
	{
		std::cout << "Error loading map" << std::endl;
		return (-1);
	}
	background.setTexture(texture2);

	if (!fontMenu.loadFromFile("rsc/verdanab.ttf"))
	{
		std::cout << "Error loading verdanab.ttf" << std::endl;
		return (-1);
	}

	sf::RectangleShape menu(sf::Vector2f(770,150));
	menu.setPosition(15.f,435.f);
	menu.setFillColor(sf::Color(0, 0, 102));
	menu.setOutlineThickness(5.f);
	menu.setOutlineColor(sf::Color::White);

	info.setFont(fontMenu);
    info.setCharacterSize(15);
    info.setPosition(200.f,480.f);
    info.setString("Ins�rez ici une histoire g�n�rique d'h�roic fantasy.");

    sf::CircleShape next(5, 3);
	next.setRotation(180);
	next.setFillColor(sf::Color(0, 0, 102));
	next.setOutlineThickness(1);
	next.setOutlineColor(sf::Color::White);
	next.setPosition(750.f,550.f);

	//Screen main loop
	while (running)
	{
		//Verifying events
		while (window.pollEvent(event))
		{
			// Window closed
			if (event.type == sf::Event::Closed)
			{
				return (-1);
			}
			if (event.type == sf::Event::LostFocus)
            {
                return (0);
            }
			//Key pressed
			if (event.type == sf::Event::KeyPressed)
			{
				switch (event.key.code)
				{
				case sf::Keyboard::Escape:
				    if (subMenu == 0)
                    {
                        return 0;
                    }

                case sf::Keyboard::Space:
                    if (subMenu == 0)
                    {
                        return (2);
                    }

                case sf::Keyboard::Return:
                    switch(subMenu)
                    {
                        case 1:
                        {
                            subMenu = 2;
                            info.setString("Allons chasser des poulets dans la for�t !\n#PasLeTempsD'�crireUnSc�nar");
                            break;
                        }
                        case 2:
                        {
                            subMenu = 0;
                            break;
                        }
                    break;
                    }

				case sf::Keyboard::Up:

				    if (subMenu == 0)
                    {
                        //Checks next position before updating it
                        nextPosy -= movement_step;

                        if(tiles[(int)((posx)/25)][(int)((nextPosy)/25)] == 1)
                        {
                            posy = nextPosy;
                        }

                        if(tiles[(int)((posx)/25)][(int)((nextPosy)/25)] == 4)
                        {
                            tiles[(int)((posx)/25)][(int)((nextPosy)/25)] = 1;
                            posy = nextPosy;
                            return 3;
                        }

                        if (!texture.loadFromFile("rsc/CharaUp.png"))
                        {
                            std::cout << "Error loading chara" << std::endl;
                            return (-1);
                        }
                        chara.setTexture(texture);

                        break;
                    }

				case sf::Keyboard::Down:

				    if (subMenu == 0)
                    {
                        //Checks next position before updating it
                        nextPosy += movement_step;

                        if(tiles[(int)((posx)/25)][(int)((nextPosy)/25)] == 1)
                        {
                            posy = nextPosy;
                        }
                        if(tiles[(int)((posx)/25)][(int)((nextPosy)/25)] == 4)
                        {
                            tiles[(int)((posx)/25)][(int)((nextPosy)/25)] = 1;
                            posy = nextPosy;
                            return 3;
                        }

                        if (!texture.loadFromFile("rsc/CharaDown.png"))
                        {
                            std::cout << "Error loading chara" << std::endl;
                            return (-1);
                        }
                        chara.setTexture(texture);

                        break;
                    }

				case sf::Keyboard::Left:

				    if (subMenu == 0)
                    {
                        //Checks next position before updating it
                         nextPosx -= movement_step;

                        if(tiles[(int)((nextPosx)/25)][(int)((posy)/25)] == 1)
                        {
                            posx = nextPosx;
                        }
                        if(tiles[(int)((nextPosx)/25)][(int)((posy)/25)] == 4)
                        {
                            tiles[(int)((nextPosx)/25)][(int)((posy)/25)] = 1;
                            posx = nextPosx;
                            return 3;
                        }

                        if (!texture.loadFromFile("rsc/CharaLeft.png"))
                        {
                            std::cout << "Error loading chara" << std::endl;
                            return (-1);
                        }
                        chara.setTexture(texture);

                        break;
                    }

				case sf::Keyboard::Right:

				    if (subMenu == 0)
                    {
                        //Checks next position before updating it
                        nextPosx += movement_step;

                        if(tiles[(int)((nextPosx)/25)][(int)((posy)/25)] == 1)
                        {
                            posx = nextPosx;
                        }
                        if(tiles[(int)((nextPosx)/25)][(int)((posy)/25)] == 4)
                        {
                            tiles[(int)((nextPosx)/25)][(int)((posy)/25)] = 1;
                            posx = nextPosx;
                            return 3;
                        }

                        if (!texture.loadFromFile("rsc/CharaRight.png"))
                        {
                            std::cout << "Error loading chara" << std::endl;
                            return (-1);
                        }
                        chara.setTexture(texture);

                        break;
                    }

				default:
					break;

				}
			}
		}

		//Updating
		if (posx>790)
			posx = 790;
		if (posx<0)
			posx = 0;
		if (posy>590)
			posy = 590;
		if (posy<0)
			posy = 0;

		chara.setPosition(posx, posy);

		//Clearing screen
		window.clear(sf::Color(0, 0, 0, 0));

		//Drawing (map first to see chara on top)
		window.draw(background);
		window.draw(chara);

		if (subMenu != 0)
        {
            window.draw(menu);
            window.draw(info);
            window.draw(next);
        }

		window.display();
	}

	//Never reaching this point normally, but just in case, exit the application
	return -1;
}
