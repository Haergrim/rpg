#ifndef MENUGUI_H
#define MENUGUI_H

#include <iostream>
#include <sstream>
#include "cScreen.h"

#include <SFML/Graphics.hpp>

class MainGame;


/// \class MenuGUI MenuGUI.h
/// \brief Class used to generate the in game menu screen
class MenuGUI : public cScreen
{
    public:
        MenuGUI(MainGame *RPG);
        virtual ~MenuGUI();
        virtual int run(sf::RenderWindow &window);
        void showStats();
        void showEquip();
        void showInv();
        void showParam();
        sf::String iToStr(int i);

    protected:

    private:
        float posx;
        float posy;
        float posxInv;
        float posyInv;
        float movement_step;
        float movement_step2;
        MainGame *RPG;
        int subMenu;

        //Basic menu
        sf::CircleShape cursor;
        sf::Font fontMenu;
        sf::Texture texture;
        sf::Sprite background;
        sf::Text menu1;
        sf::Text menu2;
        sf::Text menu3;
        sf::Text menu4;
        sf::Text timer;

        //Stats submenu
        sf::Text charaName;
        sf::Text charaStats;
        sf::Sprite charaPicture;
        sf::Texture charaImage;

        //Equip subMenu
        sf::Text charaWeap;
        sf::Text charaArmor;
        sf::Text weapDescrpt;
        sf::Text armorDescrpt;

        //nventory subMenu
        sf::Text inventoryWeap;
        sf::Text inventoryArmor;
        sf::Text inventoryMisc;
        std::vector <sf::Text> showInventoryMisc;
        sf::CircleShape cursorInv;

        //Params subMenu
        sf::Text param;

};

#endif // MENUGUI_H
