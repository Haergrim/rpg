#include "Weapon.h"

/// \fn Weapon()
/// \brief Initializing a basic weapon
Weapon::Weapon()
{
    //ctor
    name = "Excaliborg";
    description = "The game's strongest weapon";
    damages = 10;
}

Weapon::~Weapon()
{
    //dtor
}

/// \fn int getDmg()
/// \brief Returns the weapon's amount of damages
int Weapon::getDmg()
{
    return damages;
}
