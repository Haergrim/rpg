#include "MainGame.h"
#include <stdio.h>

#include "Monster.h"


/// \fn MainGame()
/// \brief Constructor initializing the main playable character when game is starting
MainGame::MainGame()
{
    //ctor
    player = new PC();
    clock.restart();
}

MainGame::~MainGame()
{
    //dtor
    delete player;
}

/// \fn int launchGame()
/// \brief Function managing the main windows and all game screens
int MainGame::launchGame()
{
    sf::ContextSettings settings;
    settings.antialiasingLevel = 16;

    sf::RenderWindow window(sf::VideoMode(800, 600), "Uber RPG", sf::Style::Default, settings);
    window.setFramerateLimit(60);

    std::vector<cScreen*> screens;
	int screen = 0;

	window.setMouseCursorVisible(false);

	//Screens preparations
	MainMenu s0;
	screens.push_back(&s0);
	GameWindow s1;
	screens.push_back(&s1);
	MenuGUI *s2 = new MenuGUI(this);
	screens.push_back(s2);
	Fight *s3 = new Fight(this);
	screens.push_back(s3);
	GameOver s4;
	screens.push_back(&s4);

	//Main loop
	while (screen >= 0)
	{
		screen = screens[screen]->run(window);
	}

	return 0;
}

