#ifndef WEAPON_H
#define WEAPON_H

#include "Item.h"

/// \class Weapon Weapon.h
/// \brief Class used for chara's weapon
class Weapon : public Item
{
    public:
        Weapon();
        virtual ~Weapon();

        int getDmg();

    protected:

    private:
        int damages;
};

#endif // WEAPON_H
