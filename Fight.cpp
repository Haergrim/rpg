#include "Fight.h"
#include "MainGame.h"
#include <sstream>

/// \fn Fight (MainGame *RPG)
/// \brief Constructor takes a MainGame pointer in argument in order to pass information from one class to the other.
Fight::Fight(MainGame *RPG)
{
	movement_step = 30;
	this->RPG = RPG;
}

Fight::~Fight()
{
    //dtor
    delete RPG;
}

/// \fn int run(sf::RenderWindow &window)
/// \brief Creates the fight screen and manages it's logic
int Fight::run(sf::RenderWindow &window)
{
    //Reinitialize cursors position and monster for each new fight (everytime this window is called)
    monsterInFight = new Monster();
    posx = 40;
	posy = 455;
	posx2 = 170;
	posy2 = 455;
	subMenu = 0;
	sf::Event event;
	bool running = true;
	int choice = 0;
	bool musicOn = true;

	//Window visual elements
	sf::Texture textureBack;
	sf::Texture textureP;
	sf::Texture textureM;
	sf::Texture textureMute;
	sf::Sprite background;
	sf::Sprite perso;
	sf::Sprite monstre;
	sf::Sprite mute;
	sf::Font fontMenu;
	sf::Text menus[4];
	sf::Text info;
	sf::Text subMenus[3];

	//Charging music and textures
    sf::Music music;
    if (!music.openFromFile("rsc/Music/CyaronsGate.ogg"))
        return -1; // erreur
    music.setLoop(true);
    music.play();

    if (!textureBack.loadFromFile("rsc/Background-fight.png"))
	{
		std::cout << "Error loading fight background" << std::endl;
		return (-1);
	}

	background.setTexture(textureBack);

	if (!fontMenu.loadFromFile("rsc/verdanab.ttf"))
	{
		std::cout << "Error loading verdanab.ttf" << std::endl;
		return (-1);
	}

	if (!textureP.loadFromFile("rsc/CharaFight.png"))
	{
		std::cout << "Error loading perso" << std::endl;
		return (-1);
	}
	perso.setTexture(textureP);
	perso.setPosition(650.f, 225.f);

	if(monsterInFight->getName() == "Chocobo Bleu")
    {
        if (!textureM.loadFromFile("rsc/Monster1.png"))
        {
            std::cout << "Error loading monstre" << std::endl;
            return (-1);
        }
    }

    if(monsterInFight->getName() == "Chocobo Or")
    {
        if (!textureM.loadFromFile("rsc/Monster2.png"))
        {
            std::cout << "Error loading monstre" << std::endl;
            return (-1);
        }
    }

    if(monsterInFight->getName() == "Chocobo Vert")
    {
        if (!textureM.loadFromFile("rsc/Monster3.png"))
        {
            std::cout << "Error loading monstre" << std::endl;
            return (-1);
        }
    }

    if (!textureMute.loadFromFile("rsc/mute.png"))
	{
		std::cout << "Error loading mainmenu background" << std::endl;
		return (-1);
	}

	mute.setTexture(textureMute);
	mute.setPosition(750.f,20.f);

    //Setting graphic elements
	monstre.setTexture(textureM);
	monstre.setPosition(100.f,225.f);

	sf::CircleShape cursor(10, 3);
	cursor.setRotation(90);
	cursor.setFillColor(sf::Color::Yellow);

	sf::RectangleShape menu(sf::Vector2f(770,150));
	menu.setPosition(15.f,435.f);
	menu.setFillColor(sf::Color(0, 0, 102));
	menu.setOutlineThickness(5.f);
	menu.setOutlineColor(sf::Color::White);

	for (int i = 0 ; i < 4 ; i++)
    {
        menus[i].setFont(fontMenu);
        menus[i].setCharacterSize(15);
        menus[i].setString("Attack");
        menus[i].setPosition(50.f, 455+i*movement_step);
    }

    menus[0].setString("Attack");
    menus[1].setString("Defend");
    menus[2].setString("Use");
    menus[3].setString("Run");

	info.setFont(fontMenu);
    info.setCharacterSize(15);
    info.setPosition(400.f,480.f);

    infos = RPG->player->getName() + "\n\nHP : " + iToStr(RPG->player->getCurrentHP()) + " / " + iToStr(RPG->player->getMaxHP());

    sf::CircleShape cursorDef(10, 3);
	cursorDef.setRotation(90);
	cursorDef.setFillColor(sf::Color::Yellow);

	sf::RectangleShape subMenu1(sf::Vector2f(150,130));
	subMenu1.setPosition(150.f,445.f);
	subMenu1.setFillColor(sf::Color(0, 0, 102));
	subMenu1.setOutlineThickness(5.f);
	subMenu1.setOutlineColor(sf::Color::White);

    for (int i = 0 ; i < 3 ; i++)
    {
        subMenus[i].setFont(fontMenu);
        subMenus[i].setCharacterSize(15);
        subMenus[i].setPosition(180.f, 455+i*movement_step);
    }

    subMenus[0].setString("Haut");
    subMenus[1].setString("Droit");
    subMenus[2].setString("Gauche");

    sf::CircleShape next(8, 3);
	next.setRotation(180);
	next.setFillColor(sf::Color(0, 0, 102));
	next.setOutlineThickness(1);
	next.setOutlineColor(sf::Color::White);
	next.setPosition(750.f,550.f);

	//Window main loop
	while (running)
	{
		//Verifying events
		while (window.pollEvent(event))
		{
			// Window closed
			if (event.type == sf::Event::Closed)
			{
				return (-1);
			}
			//Key pressed
			if (event.type == sf::Event::KeyPressed)
			{
				switch (event.key.code)
				{
                case sf::Keyboard::M:
                    if (musicOn)
                    {
                        music.stop();
                        musicOn = false;
                    }
                    else
                    {
                        music.play();
                        musicOn = true;
                    }
                    break;

                case sf::Keyboard::Escape:
                    if (subMenu == 2)
                    {
                        subMenu = 0;
                        infos = RPG->player->getName() + "\n\nHP : " + iToStr(RPG->player->getCurrentHP()) + " / " + iToStr(RPG->player->getMaxHP());
                    }

                    break;

                case sf::Keyboard::Up:
                    if (subMenu == 0)
                    {
                        if (posy <= 460)
                            posy = 545;
                        else
                            posy -= movement_step;
                    }
                    if (subMenu == 2)
                    {
                        if (posy2 <= 460)
                            posy2 = 515;
                        else
                            posy2 -= movement_step;
                    }

                    break;

                case sf::Keyboard::Down:
                    if (subMenu == 0)
                    {
                        if (posy >= 530)
                            posy = 455;
                        else
                            posy += movement_step;
                    }
                    if (subMenu == 2)
                    {
                        if (posy2 >= 510)
                            posy2 = 455;
                        else
                            posy2 += movement_step;
                    }

                    break;

                case sf::Keyboard::Return:
                    {
                        //In order to show each text line properly, each action send to another "subMenu" in which the Return key has a specific comportment
                        switch (subMenu)
                        {
                        //Base situation
                        case 0 :
                            if (posy == 455)
                            {
                                subMenu = 1;
                                attack();
                                break;
                            }
                            if(posy == 485)
                            {
                                subMenu = 2;
                                infos = "L'ennemi s'appr�te � frapper.\n Quel endroit voulez-vous prot�ger ?";
                                break;
                            }
                            if(posy == 515)
                            {
                                use();
                                subMenu = 3;
                                break;
                            }
                            if(posy == 545)
                            {
                                if (rand()%10+1 < (RPG->player->getDex()+10-monsterInFight->getDex()))
                                {
                                    infos = "Vous fuyez le combat";
                                    subMenu = 7;
                                }

                                else
                                {
                                    infos = "Vous ne parvenez pas � fuir";
                                    subMenu = 1;
                                }

                                break;
                            }
                        //Attack option chosen
                        case 1:
                            if (monsterInFight->getCurrentHP() <= 0)
                            {
                                subMenu = 7;
                                infos = "Le monstre est mort";
                            }
                            else
                            {
                                subMenu = 6;
                                attackMonster();
                                if (RPG->player->getCurrentHP() <=0)
                                    subMenu = 4;
                            }

                            break;
                        //Defend option chosen
                        case 2:
                            {
                            if (posy2 == 455)
                                choice = 1;

                            if(posy2 == 485)
                                choice = 2;

                            if(posy2 == 515)
                                choice = 3;

                            if (choice == rand()%3+1)
                                defend();
                            else
                                infos = "Le monstre passe votre garde.";

                            subMenu = 1;
                            }

                            break;
                        //Back to normal atm, future use item
                        case 3:
                            subMenu = 0;
                            infos = RPG->player->getName() + "\n\nHP : " + iToStr(RPG->player->getCurrentHP()) + " / " + iToStr(RPG->player->getMaxHP());
                            break;
                        //Display chara dead
                        case 4:
                            infos = "Vous �tes mort.";
                            subMenu = 5;
                            break;
                        //Leaving on chara dead, directing to GameOver screen
                        case 5:
                            delete monsterInFight;
                            return 4;
                        //Back to normal
                        case 6:
                            subMenu = 0;
                            infos = RPG->player->getName() + "\n\nHP : " + iToStr(RPG->player->getCurrentHP()) + " / " + iToStr(RPG->player->getMaxHP());
                            break;
                        //Winning experience
                        case 7:
                            subMenu = 8;
                            RPG->player->addXP(monsterInFight->getGivenXp());
                            infos = "Vous gagnez " + iToStr(monsterInFight->getGivenXp()) + " xp !";
                            break;
                        //Monster Dead, end of fight, return to main screen
                        case 8 :
                            delete monsterInFight;
                            return 1;
                        }
                    }
                }
            }
		}

		//Update
		cursor.setPosition(posx, posy);
		cursorDef.setPosition(posx2, posy2);
		info.setString(infos);

		//Clearing screen
		window.clear();

		//Drawing elements
		window.draw(background);
		window.draw(perso);
		window.draw(monstre);
		window.draw(menu);
		for (int i = 0 ; i < 4 ; i++)
        {
            window.draw(menus[i]);
        }

        if (subMenu == 0)
        {
            window.draw(cursor);
        }
        if (subMenu == 2)
        {
            window.draw(subMenu1);
            window.draw(cursorDef);
            for (int i = 0 ; i < 5 ; i++)
            {
               window.draw(subMenus[i]);
            }
        }
        if (subMenu != 0)
        {
            window.draw(next);
        }
        if (!musicOn)
        {
            window.draw(mute);
        }

        window.draw(info);

        //Display everything
		window.display();
	}

	//Never reaching this point normally, but just in case, exit the application
	return (-1);
}

/// \fn void attack()
/// \brief Determines if the playable chara touches the monster and how much damages is done
void Fight::attack()
{
    if(rand()%10+1 < (RPG->player->getDex()+10-monsterInFight->getDex()))
    {
        monsterInFight->looseHP(RPG->player->getStr()+ RPG->player->getEquipWeapon().getDmg()- monsterInFight->getCons());
        infos = "Vous touchez et infligez : " + iToStr(RPG->player->getStr()+ RPG->player->getEquipWeapon().getDmg()- monsterInFight->getCons()) + " d�g�ts";
    }
    else
    {
        infos = "Vous ratez votre coup";
    }
}

/// \fn void attackMonster()
/// \brief Determines if the monster touches the playable chara and how much damages is done
void Fight::attackMonster()
{
    if(rand()%10+1 < (monsterInFight->getDex()+10-RPG->player->getDex()))
    {
        RPG->player->looseHP(monsterInFight->getStr()- RPG->player->getEquipArmor().getDef() - RPG->player->getCons());
        infos = "Le monstre touche et inflige : " + iToStr(monsterInFight->getStr()- RPG->player->getEquipArmor().getDef() - RPG->player->getCons()) + " d�g�ts";
    }
    else
    {
        infos = "Le monstre rate son coup";
    }
}

/// \fn void monsterDead()
/// \brief Direct to the subMenu used for monster death
void Fight::monsterDead()
{
    infos = "Le monstre est mort";
    subMenu = 7;
}

/// \fn void playerDead()
/// \brief Direct to the subMenu used for playable chara death
void Fight::playerDead()
{
    infos = "Vous �tes mort";
    subMenu = 5;
}

/// \fn void defend()
/// \brief Damages done in case of defense and text to show
void Fight::defend()
{
    infos = "Vous parez le coup.\nVotre riposte inflige : " + iToStr((RPG->player->getStr()+ RPG->player->getEquipWeapon().getDmg()- monsterInFight->getCons())*2) + " d�g�ts.";
    monsterInFight->looseHP((RPG->player->getStr()+ RPG->player->getEquipWeapon().getDmg()- monsterInFight->getCons())*2);
}

/// \fn void use()
/// \brief Use item in fight
void Fight::use()
{
    //A coder
    infos = "Pas encore cod�";
}

/// \fn sf::String iToStr(int i)
/// \brief Utility function used to convert from int to sf::String through a stringstream method
sf::String Fight::iToStr(int i)
{
    std::stringstream ss;
	ss << i;
	return ss.str();
}
