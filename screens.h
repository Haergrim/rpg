#ifndef SCREENS_H
#define SCREENS_H

//Basic Screen Class
#include "cScreen.h"

//Including each screen of application
#include "MainMenu.h"
#include "GameWindow.h"
#include "MenuGUI.h"
#include "Fight.h"
#include "GameOver.h"

#endif // SCREENS_H
