#include "Armor.h"

/// \fn Armor()
/// \brief Only one armor initiated at the moment
Armor::Armor()
{
    //ctor
    name = "LeatherJeans";
    description = "A dirty jean found under your mattress";
    defense = 1;
}

Armor::~Armor()
{
    //dtor
}

/// \fn int getDef()
/// \brief Return an armor defense points
int Armor::getDef()
{
    return defense;
}
