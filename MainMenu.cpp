#include "MainMenu.h"

MainMenu::MainMenu()
{
	alpha_max = 3 * 255;
	alpha_div = 3;
	playing = false;
}

MainMenu::~MainMenu()
{
    //dtor
}

/// \fn int run(sf::RenderWindow &window)
/// \brief Creates the main menu screen and manages it's logic
int MainMenu::run(sf::RenderWindow &window)
{
    //Used variables
	sf::Event event;
	bool running = true;
	bool musicOn = true;
	int alpha = 0;
	int subMenu = 0;

	//Rendering elements
	sf::Texture texture;
	sf::Texture texture2;
	sf::Sprite background;
	sf::Sprite mute;
	sf::Font fontMenu;
	sf::Font fontTitle;
	sf::Text title;
	sf::Text menu[3];

	//Charging music, sprites and text
	sf::Music music;
    if (!music.openFromFile("rsc/Music/1-01 Town of Wishes.ogg"))
        return -1; // erreur
    music.setLoop(true);
    music.play();

	if (!texture.loadFromFile("rsc/MainMenu.png"))
	{
		std::cout << "Error loading mainmenu background" << std::endl;
		return (-1);
	}

	background.setTexture(texture);
	background.setColor(sf::Color(255, 255, 255, alpha));

	if (!fontMenu.loadFromFile("rsc/verdanab.ttf"))
	{
		std::cout << "Error loading verdanab.ttf" << std::endl;
		return (-1);
	}

	if (!fontTitle.loadFromFile("rsc/Rurintania.ttf"))
	{
		std::cout << "Error loading Rurintania.ttf" << std::endl;
		return (-1);
	}

	if (!texture2.loadFromFile("rsc/mute.png"))
	{
		std::cout << "Error loading mainmenu background" << std::endl;
		return (-1);
	}

	mute.setTexture(texture2);
	mute.setPosition(750.f,20.f);

	title.setFont(fontTitle);
	title.setCharacterSize(40);
	title.setString("Chasse au poulet de combat");
	title.setPosition (180.f,80.f);
	title.setFillColor(sf::Color(0, 0, 0));

	for (int i = 0 ; i < 3 ; i++)
    {
        menu[i].setFont(fontMenu);
        menu[i].setCharacterSize(20);
    }

	menu[0].setString("Nouvelle partie");
	menu[0].setPosition(280.f, 260.f);

	menu[1].setString("Exit");
	menu[1].setPosition(280.f, 320.f);

	menu[2].setString("Continue");
	menu[2].setPosition(280.f, 260.f);

	if (playing)
	{
		alpha = alpha_max;
	}

	while (running)
	{
		//Verifying events
		while (window.pollEvent(event))
		{
			// Window closed
			if (event.type == sf::Event::Closed)
			{
				return (-1);
			}
			//Key pressed
			if (event.type == sf::Event::KeyPressed)
			{
				switch (event.key.code)
				{
                case sf::Keyboard::M:
                    if (musicOn)
                    {
                        music.stop();
                        musicOn = false;
                    }
                    else
                    {
                        music.play();
                        musicOn = true;
                    }
                    break;

				case sf::Keyboard::Up:
					subMenu = 0;
					break;
				case sf::Keyboard::Down:
					subMenu = 1;
					break;
				case sf::Keyboard::Return:
					if (subMenu == 0)
					{
						//Let's play !
						playing = true;
						return (1);
					}
					else
					{
						//Let's get to work...
						return (-1);
					}
					break;
				default:
					break;
				}
			}
		}

		//When getting at alpha_max, we stop modifying the sprite
		if (alpha<alpha_max)
		{
			alpha+=5;
		}

		//Used to add a fade effect on main menu
		background.setColor(sf::Color(255, 255, 255, alpha / alpha_div));

		if (subMenu == 0)
		{
			menu[0].setFillColor(sf::Color(255, 0, 0, 255));
			menu[1].setFillColor(sf::Color(255, 255, 255, 255));
			menu[2].setFillColor(sf::Color(255, 0, 0, 255));
		}
		else
		{
			menu[0].setFillColor(sf::Color(255, 255, 255, 255));
			menu[1].setFillColor(sf::Color(255, 0, 0, 255));
			menu[2].setFillColor(sf::Color(255, 255, 255, 255));
		}

		//Clearing screen
		window.clear();

		//Drawing
		window.draw(background);

		if (alpha == alpha_max)
		{
			if (playing)
			{
				window.draw(menu[2]);
			}
			else
			{
			    window.draw(title);
				window.draw(menu[0]);
			}
			window.draw(menu[1]);
			if (!musicOn)
            {
                window.draw(mute);
            }
		}
		window.display();
	}

	//Never reaching this point normally, but just in case, exit the application
	return (-1);
}
