#ifndef GAMEOVER_H
#define GAMEOVER_H

#include "cScreen.h"
#include <iostream>

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

/// \class GameOver GameOver/h
/// \brief Class creating and managing the GameOver screen
class GameOver : public cScreen
{
    public:
        GameOver();
        virtual ~GameOver();
        virtual int run(sf::RenderWindow &window);

    protected:

    private:
        sf::Sprite background;
};

#endif // GAMEOVER_H
