#include "Loot.h"

/// \fn Loot()
/// \brief Constructor initiating potions at the moment
Loot::Loot()
{
    //ctor
    dropChance = 100;
    name = "Potion";
    healingValue = 50;
    description = "A small healing potion";
    value = 5;
}

Loot::~Loot()
{
    //dtor
}

/// \fn int getDropChance()
/// \brief Returns the chance you have to drop an item
int Loot::getDropChance()
{
    return dropChance;
}
