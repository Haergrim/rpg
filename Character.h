#ifndef CHARACTER_H
#define CHARACTER_H

#include <SFML/Graphics.hpp>

/// \class Character Character.h
/// \brief A class describing all types of characters, used as a mother class for both PC and Monster classes
class Character
{
    public:
        Character();
        virtual ~Character();

        sf::String getName();
        void setName(sf::String name);

        int getLvl();
        void lvlUp();

        int getMaxHP();
        int getMaxMP();
        int getCurrentHP();
        int getCurrentMP();

        int getStr();
        int getCons();
        int getDex();
        int getInt();
        int getExp();

        int getPosX();
        int getPosY();
        void setPos(int X, int Y);

        void looseHP(int dmg);

    protected:
        sf::String name;
        int level;
        int maxHP;
        int currentHP;
        int maxMP;
        int currentMP;
        int strength;
        int constitution;
        int dexterity;
        int intelligence;
        int experience;
        int posX;
        int posY;

    private:
};

#endif // CHARACTER_H
