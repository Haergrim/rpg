#include "GameOver.h"

/// \fn iGameOver()
/// \brief Empty constructor
GameOver::GameOver()
{
    //ctor
}

GameOver::~GameOver()
{
    //dtor
}

/// \fn int run(sf::RenderWindow &window)
/// \brief Creates the GameOver screen and manages it's logic
int GameOver::run(sf::RenderWindow &window)
{
    //Used variables
    sf::Event event;
	bool running = true;

	//Charging music and background
	sf::Music music;
    if (!music.openFromFile("rsc/Music/1-09 Peaceful Soul.ogg"))
        return -1; // erreur
    music.setLoop(true);
    music.play();

	sf::Texture texture;
	if (!texture.loadFromFile("rsc/game-over.png"))
	{
		std::cout << "Error loading chara" << std::endl;
		return (-1);
	}
	background.setTexture(texture);

    while (running)
    {
        //Verifying events
        while (window.pollEvent(event))
        {
            // Window closed
            if (event.type == sf::Event::Closed)
            {
                return (-1);
            }
            //Key pressed
            if (event.type == sf::Event::KeyPressed)
            {
                switch (event.key.code)
				{
				case sf::Keyboard::Escape:
					return -1;
                case sf::Keyboard::Return:
                    return -1;
                default :
                    break;
				}
            }
        }

        //Draw elements
        window.draw(background);

        //Display everything
        window.display();
    }
}
