#ifndef CSCREEN_H
#define CSCREEN_H

#include <SFML/Graphics.hpp>

/// \class cScreen cScreen.h
/// \brief Template class used to create new screens
class cScreen
{
    public:
        virtual int run (sf::RenderWindow &window) = 0;

    protected:

    private:
};

#endif // CSCREEN_H
