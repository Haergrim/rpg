#include "MenuGUI.h"
#include "MainGame.h"

/// \fn MenuGUI (MainGame *RPG)
/// \brief Constructor takes a MainGame pointer in argument in order to pass information from one class to the other.
MenuGUI::MenuGUI(MainGame *RPG)
{
    subMenu = 0;
	movement_step = 60;
	movement_step2 = 40;
	posx = 620;
	posy = 45;
	posxInv = 50;
	posyInv = 130;
	this->RPG = RPG;

	fontMenu.loadFromFile("rsc/verdanab.ttf");
	texture.loadFromFile("rsc/Menu.png");
	background.setTexture(texture);

	cursor.setRadius(10);
	cursor.setPointCount(3);
	cursor.setRotation(90);
	cursor.setFillColor(sf::Color::Yellow);

	menu1.setFont(fontMenu);
	menu1.setCharacterSize(20);
	menu1.setString("Stats");
	menu1.setPosition({ 650.f, 40.f });

	menu2.setFont(fontMenu);
	menu2.setCharacterSize(20);
	menu2.setString("Equipment");
	menu2.setPosition({ 650.f, 100.f });

	menu3.setFont(fontMenu);
	menu3.setCharacterSize(20);
	menu3.setString("Inventory");
	menu3.setPosition({ 650.f, 160.f });

	menu4.setFont(fontMenu);
	menu4.setCharacterSize(20);
	menu4.setString("Parameters");
	menu4.setPosition({ 650.f, 220.f });
}

MenuGUI::~MenuGUI()
{
    //dtor
    delete RPG;
}

/// \fn int run(sf::RenderWindow &window)
/// \brief Creates the in game menu screen and manages it's logic
int MenuGUI::run(sf::RenderWindow &window)
{
	sf::Event event;
	bool running = true;

	while (running)
	{
	    //Calculate and display elapsed time since game was launched
	    sf::Time elapsed = RPG->clock.getElapsedTime();
        int timeMinutes = elapsed.asSeconds()/60;
        int timeSeconds = elapsed.asSeconds()-60*timeMinutes;

        timer.setFont(fontMenu);
        timer.setCharacterSize(10);
        timer.setString(iToStr(timeMinutes) + " : " + iToStr(timeSeconds));
        timer.setPosition({ 20.f, 570.f });

		//Verifying events
		while (window.pollEvent(event))
		{
			// Window closed
			if (event.type == sf::Event::Closed)
			{
				return (-1);
			}
			//Key pressed
			if (event.type == sf::Event::KeyPressed)
			{
				switch (event.key.code)
				{
                case sf::Keyboard::Escape:
                    if (subMenu == 3 || subMenu ==4)
                    {
                        subMenu = 0;
                    }
                    else
                    {
                        return 1;
                    }

				    break;

                case sf::Keyboard::Up:
                    if(subMenu == 3)
                    {
                        if (posyInv <= 120)
                            posyInv = 370;
                        else
                            posyInv -= movement_step2;
                    }
                    else
                    {
                        if (posy <= 45)
                            posy = 220;
                        else
                            posy -= movement_step;
                    }

                    break;

                case sf::Keyboard::Down:
                    if(subMenu == 3)
                    {
                        if (posyInv >= 380)
                            posyInv = 130;
                        else
                            posyInv += movement_step2;
                    }
                    else
                    {
                        if (posy >= 220)
                            posy = 45;
                        else
                            posy += movement_step;
                    }

                    break;

                case sf::Keyboard::Return:
                    if (posy == 45)
                        showStats();
                    if (posy == 105)
                        showEquip();
                    if (posy == 165)
                        showInv();
                    if (posy == 225)
                        showParam();

                    break;

				default:
					break;
				}
			}
		}

		//Updating cursors position
		cursor.setPosition(posx, posy);
		cursorInv.setPosition(posxInv,posyInv);

		//Clearing screen
		window.clear();

		//Drawing main elements of the menu
		window.draw(background);
		window.draw(menu1);
        window.draw(menu2);
        window.draw(menu3);
        window.draw(menu4);
        window.draw(timer);

        //Drawing other elements based on which sub-menu is open (0 : main elements, 1 : stats, 2 : equipment, 3 : inventory)
        if (subMenu == 0 || subMenu == 1 || subMenu == 2)
        window.draw(cursor);

        switch(subMenu)
        {
        case 1:
            window.draw(charaName);
            window.draw(charaStats);
            window.draw(charaPicture);
            break;
        case 2:
            window.draw(charaWeap);
            window.draw(weapDescrpt);
            window.draw(charaArmor);
            window.draw(armorDescrpt);
            break;
        case 3:
            window.draw(cursorInv);
            window.draw(inventoryWeap);
            window.draw(inventoryArmor);
            window.draw(inventoryMisc);
            for (int i = 0 ; i < showInventoryMisc.size() ; i++)
            {
                window.draw(showInventoryMisc[i]);
            }
            break;
        case 4:
            window.draw(param);
            break;
        }

		window.display();
	}

	//Never reaching this point normally, but just in case, exit the application
	return (-1);
}

/// \fn void showStats()
/// \brief Sets stats subMenu elements to show
void MenuGUI::showStats()
{
    subMenu = 1;

    charaImage.loadFromFile("rsc/CharaBig.png");
	charaPicture.setTexture(charaImage);
	charaPicture.setPosition(100.f,120.f);

	charaName.setFont(fontMenu);
	charaName.setCharacterSize(20);
	charaName.setString(RPG->player->getName());
	charaName.setPosition({ 250.f, 100.f });

    charaStats.setFont(fontMenu);
	charaStats.setCharacterSize(15);
	charaStats.setString("Level : " + iToStr(RPG->player->getLvl()) + "\n\nHP : " + iToStr(RPG->player->getMaxHP()) + "\nMP : " + iToStr(RPG->player->getMaxMP()) + "\nStrength : " + iToStr(RPG->player->getStr()) + "\nConstitution : " + iToStr(RPG->player->getCons()) + "\nDexterity : " + iToStr(RPG->player->getDex()) + "\nIntelligence : " + iToStr(RPG->player->getInt()) + "\nExperience : " + iToStr(RPG->player->getExp()));
	charaStats.setPosition({ 250.f, 140.f });

}

/// \fn void showEquip()
/// \brief Sets equip subMenu elements to show
void MenuGUI::showEquip()
{
    subMenu = 2;

    charaWeap.setFont(fontMenu);
    charaWeap.setCharacterSize(20);
    charaWeap.setString("Equipped material :\n\n\nWeapon : " + RPG->player->getEquipWeapon().getName());
    charaWeap.setPosition(200.f,140.f);

    weapDescrpt.setFont(fontMenu);
    weapDescrpt.setCharacterSize(10);
    weapDescrpt.setStyle(sf::Text::Italic);
    weapDescrpt.setString(RPG->player->getEquipWeapon().getDescription());
    weapDescrpt.setPosition(200.f,250.f);

    charaArmor.setFont(fontMenu);
    charaArmor.setCharacterSize(20);
    charaArmor.setString("Armor : " + RPG->player->getEquipArmor().getName());
    charaArmor.setPosition(200.f,280.f);

    armorDescrpt.setFont(fontMenu);
    armorDescrpt.setCharacterSize(10);
    armorDescrpt.setStyle(sf::Text::Italic);
    armorDescrpt.setString(RPG->player->getEquipArmor().getDescription());
    armorDescrpt.setPosition(200.f,320.f);

}

/// \fn void showInv()
/// \brief Sets inventory subMenu elements to show
void MenuGUI::showInv()
{
    subMenu = 3;

    cursorInv.setRadius(10);
    cursorInv.setPointCount(3);
	cursorInv.setRotation(90);
	cursorInv.setFillColor(sf::Color::Yellow);

	inventoryWeap.setFont(fontMenu);
    inventoryWeap.setCharacterSize(20);
    inventoryWeap.setString("Weapons : \n" + RPG->player->inventoryWeap[0]->getName());
    inventoryWeap.setPosition(70.f,100.f);

    inventoryArmor.setFont(fontMenu);
    inventoryArmor.setCharacterSize(20);
    inventoryArmor.setString("Armors : \n" + RPG->player->inventoryArmor[0]->getName());
    inventoryArmor.setPosition(70.f,200.f);

    inventoryMisc.setFont(fontMenu);
    inventoryMisc.setCharacterSize(20);
    //inventoryMisc.setString("Miscellanous : \n" + RPG->player->inventoryMisc[0]->getName());

    for (int i = 0 ; i < RPG->player->inventoryMisc.size() ; i++)
    {
        inventoryMisc.setString(RPG->player->inventoryMisc[i]->getName());
        inventoryMisc.setPosition(70.f,330+30*i);
        showInventoryMisc.push_back(inventoryMisc);
    }
    inventoryMisc.setString("Miscellanous : ");
    inventoryMisc.setPosition(70.f,300.f);
}

/// \fn void showParam()
/// \brief Sets parameters subMenu elements to show
void MenuGUI::showParam()
{
    //A coder
    subMenu = 4;

    param.setFont(fontMenu);
    param.setCharacterSize(20);
    param.setString("Pas encore cod�");
    param.setPosition(70.f,100.f);
}

/// \fn sf::String iToStr(int i)
/// \brief Utility function used to convert integer to s::String using a stringstream method
sf::String MenuGUI::iToStr(int i)
{
    std::stringstream ss;
	ss << i;
	return ss.str();
}
