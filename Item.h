#ifndef ITEM_H
#define ITEM_H

#include <SFML/Graphics.hpp>

/// \class Item Item.h
/// \brief A class describing all types of items, used as a mother class for weapon, armor and miscellanous
class Item
{
    public:
        Item();
        virtual ~Item();

        void addToInv();
        void sell();

        sf::String getName();
        sf::String getDescription();
        int getValue();

    protected:
        sf::String name;
        sf::String description;
        int value;

    private:
};

#endif // ITEM_H
