#include "Miscellanous.h"

/// \fn Miscellanous()
/// \brief Empty constructor
Miscellanous::Miscellanous()
{
    //ctor
    name = "Potion";
    description = "Small healing potion";
    healingValue = 50;
    value = 5;
}

Miscellanous::~Miscellanous()
{
    //dtor
}

/// \fn void heal()
/// \brief Adds HP to a chara based on the object healingValue (not implemented yet)
 void Miscellanous::heal()
 {
    //A coder
 }

/// \fn void buff()
/// \brief Adds buffs to a chara (not implemented yet)
void Miscellanous::buff()
{
    // A coder
}
