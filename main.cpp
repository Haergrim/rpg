#include "MainGame.h"
#include "time.h"
#include "stdlib.h"

/// \file main.cpp
/// \brief Whole game main loop
/// \author Olivier Leuillier
int main()
{
    //Main loop
    srand(time(NULL));

    MainGame *RPG = new MainGame();

    RPG->launchGame();

    delete RPG;

    return 0;
}
