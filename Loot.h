#ifndef LOOT_H
#define LOOT_H

#include "Miscellanous.h"

/// \class Loot Loot.h
/// \brief Class used to charge the droppable items on a monster
class Loot : public Miscellanous
{
    public:
        Loot();
        virtual ~Loot();

        int getDropChance();

    protected:

    private:
        int dropChance;
};

#endif // LOOT_H
