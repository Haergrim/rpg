#ifndef MAINMENU_H
#define MAINMENU_H

#include <iostream>
#include "cScreen.h"

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

/// \class MainMenu MainMenu.h
/// \brief Class used to generate the MainMenu screen
class MainMenu : public cScreen
{
    public:
        MainMenu();
        virtual ~MainMenu();
        virtual int run(sf::RenderWindow &window);

    protected:

    private:
        int alpha_max;
        int alpha_div;
        bool playing;

};

#endif // MAINMENU_H
