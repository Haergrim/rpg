#include "Monster.h"

/// \fn Monster()
/// \brief Constructor initializing one random monster in a list of three (will be picked up from database in a future release)
Monster::Monster()
{
    random = rand()%3+1;

    if (random == 1)
    {
        name = "Chocobo Bleu";
        level = 1;
        maxHP = 30;
        currentHP = maxHP;
        maxMP = 0;
        currentMP = maxMP;
        strength = 12;
        constitution = 3;
        dexterity = 10;
        intelligence = 2;
        givenExp = 50;
    }
     if (random == 2)
    {
        name = "Chocobo Or";
        level = 2;
        maxHP = 50;
        currentHP = maxHP;
        maxMP = 0;
        currentMP = maxMP;
        strength = 15;
        constitution = 5;
        dexterity = 10;
        intelligence = 2;
        givenExp = 150;
    }
     if (random == 3)
    {
        name = "Chocobo Vert";
        level = 1;
        maxHP = 10;
        currentHP = maxHP;
        maxMP = 0;
        currentMP = maxMP;
        strength = 8;
        constitution = 2;
        dexterity = 17;
        intelligence = 2;
        givenExp = 100;
    }
    //loot[0] = new Loot();
    //loot[1] = new Loot();
}

Monster::~Monster()
{
    //dtor
}

/// \fn int getGivenXp()
/// \brief Returns the amount of experience given by a monster when slain
int Monster::getGivenXp()
{
    return givenExp;
}

/// \fn sf::String getType()
/// \brief Returns the monster type (not used yet, coming soon with magical affinities)
sf::String Monster::getType()
{
    return type;
}

/// \fn Loot getLoot()
/// \brief Returns a monster loot
Loot Monster::getLoot()
{
    return loot[2];
}

/*
    //Opening db
    sqlite3 *db;
    char *zErrMsg = 0;
    int rc;
    char *sql;

    sqlite3_open("rsc/DB/Monsters.db", &db);

    //Create SQL statement
    sql = "CREATE TABLE Monsters("  \
         "ID INT PRIMARY KEY     NOT NULL," \
         "name          TEXT    NOT NULL," \
         "level           INT     NOT NULL," \
         "maxHP            INT     NOT NULL," \
         "maxMP            INT     NOT NULL," \
         "strength            INT     NOT NULL," \
         "constitution            INT     NOT NULL," \
         "dexterity            INT     NOT NULL," \
         "intelligence            INT     NOT NULL," \
         "givenExp        INT     NOT NULL);";

    //Create SQL statement
    sql = "INSERT INTO Monsters (ID,name,level,maxHP,maxMP,strength,constitution,dexterity,intelligence,givenExp) "  \
         "VALUES (1, 'Chocobo-Bleu', 1, 30, 0, 12, 3, 10, 2, 50); " \
         "INSERT INTO Monsters (ID,name,level,maxHP,maxMP,strength,constitution,dexterity,intelligence,givenExp) "  \
         "VALUES (2, 'Chocobo-Or', 2, 50, 0, 15, 5, 10, 2, 150); "     \
         "INSERT INTO Monsters (ID,name,level,maxHP,maxMP,strength,constitution,dexterity,intelligence,givenExp) "  \
         "VALUES (3, 'Chocobo-Vert', 1, 10, 0, 7, 2, 15, 2, 100 );";


    //Create SQL statement
    sql = "SELECT * from Monsters WHERE ID = 1";

    //Execute SQL statement
    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        fprintf(stdout, "Operation done successfully\n");
    }

    sqlite3_close(db);

    */
